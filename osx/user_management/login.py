def login():

    import os
    import time

    from start import start
    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #login page
    print("--------------------------")
    time.sleep(1)
    print("Welcome, User")
    time.sleep(1)
    print("--------------------------")
    time.sleep(1)
    #to-do
    #try-catch-err
    username_input = str(input("Input Username: "))
    time.sleep(1)
    print("--------------------------")
    time.sleep(1)
    password_input = str(input("Input Password: "))
    time.sleep(1)
    print("--------------------------")

    #get credentials
    password_file = open(file_path+"/osx/user_management/user0/password.txt", "r")
    password_check = str(password_file.read())
    password_file.close()

    username_file = open(file_path+"/osx/user_management/user0/username.txt", "r")
    username_check = str(username_file.read())
    username_file.close()

    #check credentials
    if username_check == username_input:       
        if password_check == password_input:
            menu()
        else:       
            print("Username Or Password Is Invalid.")
            time.sleep(3)
            start()
    else:  
        print("Username Or Password Is Invalid.")
        time.sleep(3)
        start()
