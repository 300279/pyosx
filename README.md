# Proje ¿-DOS

Kendi programları olan ve dışarıdan program ekleyip çalıştırılabilen, sıfırdan vanilla python ile yapılmış bir işletim sistemi projesi.

Projenin seçilme sebebi: Programlama'nın neredeyse her dalında yetkin hâle gelene kadar üzerine uğraşılabilecek ve eklenebilecek bir proje.

# Nasıl Kurulur?
1. Git kütüphanesini bilgisayarınıza kurun: https://git-scm.com/downloads
2. Python3'ü bilgisayarınıza kurun: https://www.python.org/downloads/
3. CMD, Git Bash, Terminator, Guake veya herhangi bir terminali açın.
4. Aşağıdaki komutları kullanarak gitlab hesabınızı bağlayın:
   - `git config --global user.name "<username>"`
   - `git config --global user.email "<e-mail>"`
5. `git clone https://gitlab.com/0Factorial0/python.git`'ile projeyi klonlayın.
6. `cd <file_name>`, `cd ..` komutlarıyla projenin içine gidin.
7. `python3 start.py` komutuyla programı başlatın.

## Proje'nin Güncel İçeriği

Özellikler:
- Dosya Sistemi [WIP]
- Kayıt-Giriş [v1.0]
- Kullanıcı Ayarları [v0.1]
- Kullanıcı Arayüzü [WIP]
- Sistemin İçinde Dolaşmak İçin Menüler [v1.0]

Öz Uygulamalar:
- Saat ve Tarih Uygulaması [WIP]
- Hesap Makinesi [WIP]
- Not Alma Uygulaması [WIP]

İkinci Parti Uygulamalar:
- Yapay Zeka Yarıştırma Simülasyonu [WIP]
- Yazı Tabanlı Rol Yapma Oyunu [WIP]
- Sayı Tahmin Oyunu [v1.0]
- Yazı Tura Oyunu [v1.0]
- Taş Kağıt Makas Oyunu [v1.0]
- Matematik Oyunu [WIP]
- Platform Oyunu [WIP]

//Emeği Geçenler:
- 0x0
- pigeon
- k1esko
- efecaglar
- xpN7
- mohurine