def start():

    import os
        
    from osx.user_management.login import login
    from osx.user_management.register import register

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #check if registered
    isRegistered_file = open(file_path+"/osx/user_management/user0/isRegistered.txt", "r")
    isRegistered = isRegistered_file.read()

    isRegistered_file.close()

    #if user is registered go to login page, or user is not registered go to register page
    if isRegistered == "1":
        login()
    else:
        register()

start()
