def coinflip():

    import os
    import time
    import random

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    user_input = "null"
    side = "null"

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #print menu
    print("--------------------------")
    print("Choose Side")
    print("--------------------------")
    print("Press 1 For Heads")
    print("Press 2 For Tails")
    print("Press 0 For Main Menu")
    print("--------------------------")

    try:
        user_input = str(input("Input: "))
        print("--------------------------")
        time.sleep(1)

        #set choosen side
        if user_input == "1":
            side = "1"
        elif user_input == "2":
            side = "2"
        elif user_input == "0":
            menu()
        else:
            #error message
            print("Invalid Input.")
            time.sleep(3)
            coinflip()
    #print error
    except:
        print("Unexpected Error.")
        time.sleep(3)
        coinflip()

    #generate coin side
    print("--------------------------")
    print("Flipping Coin...")
    print("--------------------------")
    time.sleep(2)
    random_side = str(random.randint(1,2))

    #print status
    print("--------------------------")
    if random_side == side:
        print("You Win.")
    else:
        print("You Lose.")
    print("--------------------------")

    #restart the game
    time.sleep(2)
    coinflip()
