def rock_paper_scissors():
    
    import os
    import time
    import random

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #print menu
    print("--------------------------")
    print("Welcome To The Rock Paper Scissors Game!")
    print("--------------------------")
    print("Press 1 For Rock")
    print("Press 2 For Paper")
    print("Press 3 For Scissors")
    print("Press 0 For Main Menu")
    print("--------------------------")

    #get input and check errors
    try:
        userinput0 = int(input("Input: "))
        print("--------------------------")
        choice_list = ["Paper.", "Scissors.", "Rock."]
        ai_choice = random.randint(0,2)
        user_choice = "null"

        #check choice validity
        if userinput0 == 1:
            user_choice = 2
        elif userinput0 == 2:
            user_choice = 0
        elif userinput0 == 3:
            user_choice = 1
        elif userinput0 == 0:
            menu()
        else:
            print("Input Error.")
            time.sleep(3)
            rock_paper_scissors()

        #print choices
        print("Player Choice: "+choice_list[user_choice])
        print("Computer Choice: "+choice_list[ai_choice])
        print("--------------------------")
        time.sleep(1)

        #check and print game status
        if ai_choice == user_choice:
            print("Draw.")
        elif ai_choice > user_choice:
            if ai_choice == 2 and user_choice == 0:
                print("Player Win.")
            else:
                print("Computer Win.")
        elif ai_choice < user_choice:
            if ai_choice == 0 and user_choice == 2:
                print("Computer Win.")
            else:
                print("Player Win.")
        else:
            print("Unexpected Error.")
            time.sleep(3)
            rock_paper_scissors()
        print("--------------------------")
        time.sleep(3)
        rock_paper_scissors()
    #print errors
    except TypeError:
        print("Type Error.")
        time.sleep(3)
        rock_paper_scissors()
    except:
        print("Unexpected Error.")
        time.sleep(3)
        rock_paper_scissors()
