#main function
def guess_the_number():
    
    import os
    import time
    import random

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #print menu
    print("--------------------------")
    print("Welcome To The Guess The Number!")
    print("--------------------------")
    print("Press 1 For 1-5")
    print("Press 2 For 1-10")
    print("Press 3 For 1-100")
    print("Press 0 For Main Menu")
    print("--------------------------")

    #get input and check errors
    try:
        userinput0 = int(input("Input: "))
        print("--------------------------")
        time.sleep(1)
        gametype_list = ["Easy.","Normal.","Hard."]
        gametype = "null"
        gametype_numeric = ""

        #check choice validity and generate number
        if userinput0 == 1:
            gametype = gametype_list[0]
            random_number0 = random.randint(1,5)
            gametype_numeric = " [1-5]"
            remaining_guesses = 2
        elif userinput0 == 2:
            gametype = gametype_list[1]
            random_number0 = random.randint(1,10)
            gametype_numeric = " [1-10]"
            remaining_guesses = 2
        elif userinput0 == 3:
            gametype = gametype_list[2]
            random_number0 = random.randint(1,100)
            gametype_numeric = " [1-100]"
            remaining_guesses = 5
        elif userinput0 == 0:
            menu()
        else:
            print("Input Error.")
            time.sleep(3)
            guess_the_number()

        #print game type
        print("Game Mode: "+gametype)
        time.sleep(1)
        print("--------------------------")
        print("Guess The Number"+gametype_numeric)
        print("--------------------------")
        
        try:
            userinput1 = 23
            check(remaining_guesses, userinput1, random_number0)
        #print errors
        except TypeError:
            print("Type Error.")
            time.sleep(3)
            guess_the_number()
        except:
            print("Unexpected Error.")
            time.sleep(3)
            guess_the_number()

    #print errors
    except TypeError:
        print("Type Error.")
        time.sleep(3)
        guess_the_number()
    except:
        print("Unexpected Error.")
        time.sleep(3)
        guess_the_number()

#finalization function
def check(remaining_guesses, userinput1, random_number0):

    import time

    try:
        userinput1 = int(input("Input: "))
        print("--------------------------")

    #print errors
    except TypeError:
        print("Type Error.")
        time.sleep(3)
        guess_the_number()
    except:
        print("Unexpected Error.")
        time.sleep(3)
        guess_the_number()

    if remaining_guesses == 0:
        print("You Lost")
        time.sleep(2)
        guess_the_number()

    #check choice validity and game status && print
    if userinput1 == random_number0:
        print("You Win.")
        time.sleep(2)
        guess_the_number()
    elif userinput1 < random_number0:
        bigger(remaining_guesses, userinput1, random_number0)
    elif userinput1 > random_number0:
        lower(remaining_guesses, userinput1, random_number0)
    else:
        print("Unexpected Error.")
        time.sleep(3)
        guess_the_number()

#check function variaton 1
def bigger(remaining_guesses, userinput1, random_number0):

    import time

    print("Guess Bigger")
    print("Remaining Guesses: ["+str(remaining_guesses)+"]")
    print("--------------------------")
    remaining_guesses = remaining_guesses -1
    check(remaining_guesses, userinput1, random_number0)

#check function variaton 2
def lower(remaining_guesses, userinput1, random_number0):

    import time

    print("Guess Lower")
    print("Remaining Guesses: ["+str(remaining_guesses)+"]")
    print("--------------------------")
    remaining_guesses = remaining_guesses -1
    check(remaining_guesses, userinput1, random_number0)
