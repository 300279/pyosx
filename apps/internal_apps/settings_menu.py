def settings_menu():
    
    import os
    import time

    from apps.internal_apps.settings.change_password import change_password
    from apps.internal_apps.settings.change_user_interface import change_user_interface
    from apps.internal_apps.settings.delete_user import delete_user
    from apps.internal_apps.settings.delete_all_user_data import delete_all_user_data

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #settings menu
    print("--------------------------")
    print("Select Settings")
    print("--------------------------")
    print("Press 1 For Change Password")
    print("Press 2 For Change User Inferface")
    print("Press 3 For Delete User")
    print("Press 4 For Delete All User Data")
    print("Press 0 For Main Menu")
    print("--------------------------") 
    program_type = str(input("Input: "))
    if program_type == "1":
        change_password()
    elif program_type == "2":
        change_user_interface()
    elif program_type == "3":
        delete_user()
    elif program_type == "4":
        delete_all_user_data()
    elif program_type == "0":
        menu()
    else:
        print("Error")
        time.sleep(2)
        settings_menu()