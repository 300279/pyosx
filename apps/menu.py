def menu():

    import os
    import time

    from apps.menu import menu

    from apps.internal_apps.calculator import calculator
    from apps.internal_apps.clock import clock
    from apps.internal_apps.notepad import notepad
    from apps.internal_apps.settings_menu import settings_menu

    from apps.external_apps.coinflip import coinflip
    from apps.external_apps.math_game import math_game
    from apps.external_apps.rpg import rpg
    from apps.external_apps.ai_fight import ai_fight
    from apps.external_apps.guess_the_number import guess_the_number
    from apps.external_apps.rock_paper_scissors import rock_paper_scissors
    from apps.external_apps.platform import platform

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #main menu / routing
    print("--------------------------")
    print("Select Program Type")
    print("--------------------------")
    print("Press 1 For Internal Apps")
    print("Press 2 For External Apps")
    print("--------------------------") 
    program_type = str(input("Input: "))
    if program_type == "1":
        clear()
        #internal programs menu page
        #to-do menu header
        print("--------------------------") 
        print("Press 1 For Calculator")
        print("Press 2 For Clock And Calendar")
        print("Press 3 For Notepad")
        print("Press 4 For Settings")
        print("Press 0 For Main Menu")
        print("--------------------------") 
        internal_type = str(input("Input: "))
        clear()
        if internal_type == "1":
            calculator()
        elif internal_type == "2":
            clock()
        elif internal_type == "3":
            notepad()
        elif internal_type == "4":
            settings_menu()
        elif internal_type == "0":
            menu()
        else:
            pass
    elif program_type == "2":
        clear()
        #external programs menu page
        #to-do menu header
        print("--------------------------") 
        print("Press 1 For A.I. Fight")
        print("Press 2 For Coinflip")
        print("Press 3 For Guess The Number")
        print("Press 4 For Math Game")
        print("Press 5 For Rock Paper Scissors")
        #to-do name of rpg
        print("Press 6 For %NAME OF RPG%")
        #to-do name of platform
        print("Press 7 For %NAME OF Plaftorm%")
        print("Press 0 For Main Menu")
        print("--------------------------") 
        external_type = str(input("Input: "))
        clear()
        if external_type == "1":
            ai_fight()
        elif external_type == "2":
            coinflip()
        elif external_type == "3":
            guess_the_number()
        elif external_type == "4":
            math_game()
        elif external_type == "5":
            rock_paper_scissors()
        elif external_type == "6":
            rpg()
        elif external_type == "7":
            platform()
        elif external_type == "0":
            menu()
        else:
            print("Wrong Input.")
            time.sleep(2)
            menu()
    else:
        print("Wrong Input.")
        time.sleep(2)
        menu()
